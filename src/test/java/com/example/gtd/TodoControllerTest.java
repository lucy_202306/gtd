package com.example.gtd;

import com.example.gtd.entity.Todo;
import com.example.gtd.repository.TodoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.JsonPath;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private MockMvc client;

    @Autowired
    private TodoRepository todoRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void prepareData() {
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todos_perform_get_given_todos_in_db() throws Exception {
        // given
        Todo readBook = new Todo("read 100 books", false);
        Todo doSomeSport = new Todo("do some sport", false);

        todoRepository.saveAll(List.of(readBook, doSomeSport));

        // when then
         client.perform(MockMvcRequestBuilders.get("/todos"))
                 .andExpect(MockMvcResultMatchers.status().isOk())
                 .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                 .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(readBook.getText()))
                 .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(readBook.isDone()))
                 .andExpect(MockMvcResultMatchers.jsonPath("$[1].text").value(doSomeSport.getText()))
                 .andExpect(MockMvcResultMatchers.jsonPath("$[1].done").value(doSomeSport.isDone()));;
    }

    @Test
    void should_return_todo_by_id_perform_get_given_id_and_todos_in_db() throws Exception {
        // given
        Todo readBook = new Todo("read 100 books", false);
        Todo doSomeSport = new Todo("do some sport", false);

        todoRepository.saveAll(List.of(readBook, doSomeSport));

        // when then
        client.perform(MockMvcRequestBuilders.get("/todos/{id}", readBook.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(readBook.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(readBook.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(readBook.isDone()));
    }

    @Test
    void should_return_not_found_perform_get_given_id_and_not_in_db() throws Exception {
        // given
        Todo readBook = new Todo("read 100 books", false);
        Todo doSomeSport = new Todo("do some sport", false);

        todoRepository.saveAll(List.of(readBook, doSomeSport));

        // when then
        client.perform(MockMvcRequestBuilders.get("/todos/{id}", 3))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_return_todos_perform_post_given_todo_and_todos_in_db() throws Exception {
        // given
        Todo readBook = new Todo("read 100 books", false);
        Todo doSomeSport = new Todo("do some sport", false);
        Todo newTodo = new Todo("new todo", false);

        todoRepository.saveAll(List.of(readBook, doSomeSport));

        // when then
        client.perform(MockMvcRequestBuilders.post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(newTodo)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(readBook.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(readBook.isDone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].text").value(doSomeSport.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].done").value(doSomeSport.isDone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].text").value(newTodo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].done").value(newTodo.isDone()));
    }

    @Test
    void should_return_todos_perform_put_given_todo() throws Exception {
        // given
        Todo readBook = new Todo("read 100 books", false);
        Todo doSomeSport = new Todo("do some sport", false);
        todoRepository.saveAll(List.of(readBook, doSomeSport));

        Todo updatedDoSomeSport = new Todo("don't do sport", true);

        // when then
        client.perform(MockMvcRequestBuilders.put("/todos/{id}", doSomeSport.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedDoSomeSport)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].text").value(updatedDoSomeSport.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].done").value(updatedDoSomeSport.isDone()));
    }

    @Test
    void should_return_todos_perform_delete_given_id() throws Exception {
        // given
        Todo readBook = new Todo("read 100 books", false);
        Todo doSomeSport = new Todo("do some sport", false);
        todoRepository.saveAll(List.of(readBook, doSomeSport));

        // when then
        client.perform(MockMvcRequestBuilders.delete("/todos/{id}", readBook.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)));
    }
}
