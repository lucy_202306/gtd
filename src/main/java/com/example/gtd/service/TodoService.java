package com.example.gtd.service;

import com.example.gtd.advice.GlobalControllerAdvice;
import com.example.gtd.dto.TodoCreateRequest;
import com.example.gtd.dto.TodoResponse;
import com.example.gtd.entity.Todo;
import com.example.gtd.exception.TodoNotFoundException;
import com.example.gtd.mapper.TodoMapper;
import com.example.gtd.repository.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> getTodos() {
        return todoRepository.findAll().stream()
                .map(TodoMapper::convertTodoToTodoResponse)
                .collect(Collectors.toList());
    }

    public TodoResponse getTodoById(Integer id) {
        Todo todo = findTodoById(id);
        return TodoMapper.convertTodoToTodoResponse(todo);
    }

    public List<TodoResponse> createTodo(TodoCreateRequest todoCreateRequest) {
        Todo newTodo = TodoMapper.convertTodoCreateRequestToTodo(todoCreateRequest);
        todoRepository.save(newTodo);
        return todoRepository.findAll().stream()
                .map(TodoMapper::convertTodoToTodoResponse)
                .collect(Collectors.toList());
    }

    public List<TodoResponse> updateTodo(Integer id, TodoCreateRequest todoCreateRequest) {
        Todo todo = findTodoById(id);
        if(todoCreateRequest.getText() != null) {
            todo.setText(todoCreateRequest.getText());
        }
        todo.setDone(todoCreateRequest.isDone());
        todoRepository.save(todo);
        return todoRepository.findAll().stream()
                .map(TodoMapper::convertTodoToTodoResponse)
                .collect(Collectors.toList());
    }


    private Todo findTodoById(Integer id) {
        return todoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
    }

    public List<TodoResponse> deleteTodo(Integer id) {
        Todo deleteTodo = findTodoById(id);
        if(deleteTodo != null) {
            todoRepository.delete(deleteTodo);
        }
        return todoRepository.findAll().stream()
                .map(TodoMapper::convertTodoToTodoResponse)
                .collect(Collectors.toList());
    }
}
