package com.example.gtd.controller;

import com.example.gtd.dto.TodoCreateRequest;
import com.example.gtd.dto.TodoResponse;
import com.example.gtd.service.TodoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> getTodos() {
        return todoService.getTodos();
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Integer id) {
        return todoService.getTodoById(id);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public List<TodoResponse> createTodo(@RequestBody TodoCreateRequest todoCreateRequest) {
        return todoService.createTodo(todoCreateRequest);
    }

    @PutMapping("/{id}")
    public List<TodoResponse> updateTodo(@PathVariable Integer id, @RequestBody TodoCreateRequest todoCreateRequest) {
        return todoService.updateTodo(id, todoCreateRequest);
    }

    @DeleteMapping("/{id}")
    public List<TodoResponse> deleteTodo(@PathVariable Integer id) {
        return todoService.deleteTodo(id);
    }

}
