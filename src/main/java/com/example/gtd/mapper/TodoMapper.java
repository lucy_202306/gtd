package com.example.gtd.mapper;

import com.example.gtd.dto.TodoCreateRequest;
import com.example.gtd.dto.TodoResponse;
import com.example.gtd.entity.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {

    public static TodoResponse convertTodoToTodoResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static Todo convertTodoCreateRequestToTodo(TodoCreateRequest todoCreateRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoCreateRequest, todo);
        return todo;
    }
}
