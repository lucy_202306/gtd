package com.example.gtd.advice;

public class ErrorResponse {
    private String message;
    private int id;

    public ErrorResponse(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
